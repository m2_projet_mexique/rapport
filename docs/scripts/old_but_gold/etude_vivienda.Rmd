---
title: "Etude de l'habitat"
author: "Nicolas"
date: "9 décembre 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r chargement des bibliothèques}
library(readr)
#library(survey)
library(questionr)
```

```{r chargement des données}
data2015 <- read_csv("data/traitees/persona_vivienda_zm_mexico_2015_concentration.csv", 
    col_types = cols(ID_PERSONA = col_character(), 
        ID_VIV = col_character(), LOC50K = col_character()))
```

# Type d'habitation
```{r}
# data2015
classe_habitat_LOC <- wtd.table(data2015$CODE_LOC, data2015$CLAVIVP, weights = data2015$FACTOR_VIV)
dim(classe_habitat_LOC)
```

```{r}
head(classe_habitat_LOC)
head(classe_habitat_LOC[,1:10])
```

```{r}
somme <- sum(classe_habitat_LOC[,1:10])
head(somme[,1:9])
somme
```
```{r}
# faire sommme en lignes sumrows()
# faire la somme en colonne sumcol()
# calculer les fréquences
# dcast() - p21 du fascicule -> tansformation d'une liste en matrice
```

AFC -> Clavip sommes en ligne et en colonne ont un sens => profils de municipaltés où les différents types de logement ont un poids

# Equipement de l'habitation
## Equipement en eau 
## Equipement en électricité
## Equipement ménager et électronique
# Typologie

Avec la CAH

Expliquer: régressions

Synthèse: Analyse factorielle => réduire les dimensions

Classification => diminuer le nombre de lignes

La plus utilisée : CAH (R: hclust())

Méthodes hiérarchiques ou autour des centres variables / nuées dynamiques

Ressemblances entre les individus => mesures de distance / critères d'agrégation

- plus proche voisin / saut le plus court
- distance moyenne
- moment centré d'ordre 2 => critère de Ward: minimisé la variance intra-groupe, maximiser la variance inter-groupe

Ainé et benjamin: sous-morceau agrégés, forment un noeud. numéro `2n - k + 1`

Philcarto:

Multiv -> ACP, AFC, 3 types de CAH

- CAH mesures: variables de taux centrées réduites tableau de mesures => méthode de Ward. Tableau de données hétérogènes
- CAH correspondances: tableau de contingence (type de logement) avec des stocks AFC -> profils d'individus avec la méthode du Khi2. On puet faire des familles de profils

si étude individuelle d'un variable AFC + CAH correspondance (typologie)

Si thématique plus complexe (plusieurs variables): vulnérbilité, pauvreté, climat => ensemble hétérogène de variables
1. ACP 
2. CAH brute sur le tableau de données brutes
3. AF (AFC ou ACP) + CAH sur les résumés du tableau de données (coordonnées des individus sur les axes/ facteurs) : niveau d'abstaction important

Profils mouyens => graphe des profils moyens: sur/sous représentation dans les variables des différentes classes


**Interprétaton de la distance aux moyennes** :> facilement interprétable

Cartographie: note d'analyse: varaibles utilisées, part d'inertie expliquée par le partitionnement représenté

exemple: 14 variables ACP  => 4 variable (84%) => CAH => 8 types => (69%): 1 carte synthétise 14 cartes univariées.

Analyse de données: perdre en détail pour gagner en synthèse
- 