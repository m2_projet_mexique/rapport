# chargement des donnees
persona <- read.table("/home/niko/Rstuff/M2_projet_transversal/docs/data/databases/persona.csv", header=TRUE, sep=";", na.strings="NA", dec=".", strip.white=TRUE)
vivienda <- read.table("/home/niko/Rstuff/M2_projet_transversal/docs/data/databases/vivienda.csv", header=TRUE, sep=";", na.strings="NA", dec=".", strip.white=TRUE)

# controle des donnees
str(persona)
str(vivienda)

# merge
PersViv_Mex <- merge(persona, vivienda)



