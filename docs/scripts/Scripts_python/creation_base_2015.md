
# Constitution des bases



```python
# vérifications préliminaires
import sys
sys.version
import os
os.getcwd()
```




    '/home/niko/mexique'




```python
import glob
import pandas as pd
import numpy as np
```

## création d'une liste contenant tous les fichiers du dossier CSV/


```python
liste_fichier = []

for fichier in glob.glob('CSV/*'):
    liste_fichier.append(fichier)

len(liste_fichier)
```




    64



## Séparation des fichiers de la Zone métropolitaine de Mexico


```python
liste_municipe_zm_mexico = pd.read_csv('Municipalites_ZM_1.csv', sep=',',encoding = 'utf_8', dtype={'C': object})
code_mun_zum = liste_municipe_zm_mexico['C']
```


```python
liste_ent = [] # liste de tous les ENT
liste_zm_mexico = [] # Liste des fichiers concernant la ZM de Mexico
zum = ['México', 'Distrito Federal', 'Hidalgo']
counter = 1

for file in liste_fichier:
    fichier = pd.read_csv(file, encoding = 'latin_1')
    ent = fichier.iloc[0]['NOM_ENT']
    print(counter, '. ',file, ': ', ent)
    liste_ent.append(ent)
    if ent in zum:
        liste_zm_mexico.append(file)
    counter +=1
```

    1 .  CSV/TR_PERSONA11.CSV :  Guanajuato
    2 .  CSV/TR_VIVIENDA19.CSV :  Nuevo León
    3 .  CSV/TR_PERSONA19.CSV :  Nuevo León
    4 .  CSV/TR_PERSONA02.CSV :  Baja California
    5 .  CSV/TR_PERSONA05.CSV :  Coahuila de Zaragoza
    6 .  CSV/TR_VIVIENDA02.CSV :  Baja California
    7 .  CSV/TR_VIVIENDA23.CSV :  Quintana Roo
    8 .  CSV/TR_PERSONA21.CSV :  Puebla
    9 .  CSV/TR_PERSONA27.CSV :  Tabasco
    10 .  CSV/TR_VIVIENDA08.CSV :  Chihuahua
    11 .  CSV/TR_PERSONA23.CSV :  Quintana Roo
    12 .  CSV/TR_VIVIENDA31.CSV :  Yucatán
    13 .  CSV/TR_VIVIENDA01.CSV :  Aguascalientes
    14 .  CSV/TR_PERSONA07.CSV :  Chiapas
    15 .  CSV/TR_VIVIENDA10.CSV :  Durango
    16 .  CSV/TR_VIVIENDA15.CSV :  México
    17 .  CSV/TR_VIVIENDA28.CSV :  Tamaulipas
    18 .  CSV/TR_PERSONA12.CSV :  Guerrero
    19 .  CSV/TR_VIVIENDA25.CSV :  Sinaloa
    20 .  CSV/TR_PERSONA16.CSV :  Michoacán de Ocampo
    21 .  CSV/TR_VIVIENDA21.CSV :  Puebla
    22 .  CSV/TR_VIVIENDA16.CSV :  Michoacán de Ocampo
    23 .  CSV/TR_VIVIENDA17.CSV :  Morelos
    24 .  CSV/TR_PERSONA25.CSV :  Sinaloa
    25 .  CSV/TR_PERSONA03.CSV :  Baja California Sur
    26 .  CSV/TR_VIVIENDA04.CSV :  Campeche
    27 .  CSV/TR_PERSONA31.CSV :  Yucatán
    28 .  CSV/TR_VIVIENDA26.CSV :  Sonora
    29 .  CSV/TR_VIVIENDA30.CSV :  Veracruz de Ignacio de la Llave
    30 .  CSV/TR_PERSONA29.CSV :  Tlaxcala
    31 .  CSV/TR_PERSONA26.CSV :  Sonora
    32 .  CSV/TR_VIVIENDA32.CSV :  Zacatecas
    33 .  CSV/TR_PERSONA04.CSV :  Campeche
    34 .  CSV/TR_PERSONA17.CSV :  Morelos
    35 .  CSV/TR_VIVIENDA14.CSV :  Jalisco
    36 .  CSV/TR_PERSONA24.CSV :  San Luis Potosí
    37 .  CSV/TR_VIVIENDA07.CSV :  Chiapas
    38 .  CSV/TR_PERSONA06.CSV :  Colima
    39 .  CSV/TR_VIVIENDA09.CSV :  Distrito Federal
    40 .  CSV/TR_PERSONA32.CSV :  Zacatecas
    41 .  CSV/TR_PERSONA08.CSV :  Chihuahua
    42 .  CSV/TR_VIVIENDA27.CSV :  Tabasco
    43 .  CSV/TR_PERSONA28.CSV :  Tamaulipas
    44 .  CSV/TR_PERSONA18.CSV :  Nayarit
    45 .  CSV/TR_VIVIENDA24.CSV :  San Luis Potosí
    46 .  CSV/TR_PERSONA01.CSV :  Aguascalientes
    47 .  CSV/TR_VIVIENDA13.CSV :  Hidalgo
    48 .  CSV/TR_PERSONA09.CSV :  Distrito Federal
    49 .  CSV/TR_VIVIENDA22.CSV :  Querétaro
    50 .  CSV/TR_VIVIENDA12.CSV :  Guerrero
    51 .  CSV/TR_PERSONA22.CSV :  Querétaro
    52 .  CSV/TR_VIVIENDA05.CSV :  Coahuila de Zaragoza
    53 .  CSV/TR_VIVIENDA06.CSV :  Colima
    54 .  CSV/TR_PERSONA15.CSV :  México
    55 .  CSV/TR_PERSONA14.CSV :  Jalisco
    56 .  CSV/TR_PERSONA30.CSV :  Veracruz de Ignacio de la Llave
    57 .  CSV/TR_VIVIENDA29.CSV :  Tlaxcala
    58 .  CSV/TR_VIVIENDA18.CSV :  Nayarit
    59 .  CSV/TR_VIVIENDA20.CSV :  Oaxaca
    60 .  CSV/TR_PERSONA13.CSV :  Hidalgo
    61 .  CSV/TR_VIVIENDA03.CSV :  Baja California Sur
    62 .  CSV/TR_PERSONA10.CSV :  Durango
    63 .  CSV/TR_PERSONA20.CSV :  Oaxaca
    64 .  CSV/TR_VIVIENDA11.CSV :  Guanajuato



```python
 liste_zm_mexico
```




    ['CSV/TR_VIVIENDA15.CSV',
     'CSV/TR_VIVIENDA09.CSV',
     'CSV/TR_VIVIENDA13.CSV',
     'CSV/TR_PERSONA09.CSV',
     'CSV/TR_PERSONA15.CSV',
     'CSV/TR_PERSONA13.CSV']



## Séparation des fichiers VIVIENDA et PERSONA


```python
persona_zm_mexico = [] # liste des fichiers PERSONA de la ZM de Mexico
vivienda_zm_mexico = [] # liste des fichiers VIVIENDA de la ZM de Mexico

import re
pattern = re.compile (r'TR_VIVIENDA.*')
for fichier in liste_zm_mexico:
    if pattern.search(fichier):
        vivienda_zm_mexico.append(fichier)
    else:
        persona_zm_mexico.append(fichier)
       
```


```python
for fichier in vivienda_zm_mexico :
    print(fichier)

```

    CSV/TR_VIVIENDA15.CSV
    CSV/TR_VIVIENDA09.CSV
    CSV/TR_VIVIENDA13.CSV



```python
for fichier in persona_zm_mexico :
    print(fichier)

```

    CSV/TR_PERSONA09.CSV
    CSV/TR_PERSONA15.CSV
    CSV/TR_PERSONA13.CSV


### Concaténation des fichiers


```python
# pour chaque fichier dans la liste, pandas ouvre le csv, le convertit en dataframe et concatène les fichiers
vivienda_grand_mx = pd.concat([ pd.read_csv(df, encoding = 'latin_1', 
                                            dtype={'ID_VIV': object, 
                                                   'ID_PERSONA': object,
                                                   'ENT': object, 
                                                   'MUN': object}
                                           ) for df in  vivienda_zm_mexico])
vivienda_grand_mx['CODE_MUN'] = vivienda_grand_mx['ENT'] + vivienda_grand_mx['MUN'] 
vivienda_grand_mx.shape
```




    (854816, 89)




```python
vivienda_grand_mx.head()
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ID_VIV</th>
      <th>ENT</th>
      <th>NOM_ENT</th>
      <th>MUN</th>
      <th>NOM_MUN</th>
      <th>LOC50K</th>
      <th>NOM_LOC</th>
      <th>COBERTURA</th>
      <th>ESTRATO</th>
      <th>UPM</th>
      <th>...</th>
      <th>ALIM_MEN3</th>
      <th>ING_ALIM_MEN1</th>
      <th>ING_ALIM_MEN2</th>
      <th>ING_ALIM_MEN3</th>
      <th>TAMLOC</th>
      <th>TIPOHOG</th>
      <th>JEFE_SEXO</th>
      <th>JEFE_EDAD</th>
      <th>INGTRHOG</th>
      <th>CODE_MUN</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>150010000001</td>
      <td>15</td>
      <td>México</td>
      <td>001</td>
      <td>Acambay de Ruíz Castañeda</td>
      <td>0</td>
      <td>Localidad de menos de 50 mil habitantes</td>
      <td>2</td>
      <td>15-001-09</td>
      <td>109361</td>
      <td>...</td>
      <td>6.0</td>
      <td>2.0</td>
      <td>4.0</td>
      <td>6.0</td>
      <td>1</td>
      <td>2</td>
      <td>1</td>
      <td>26</td>
      <td>NaN</td>
      <td>15001</td>
    </tr>
    <tr>
      <th>1</th>
      <td>150010000002</td>
      <td>15</td>
      <td>México</td>
      <td>001</td>
      <td>Acambay de Ruíz Castañeda</td>
      <td>0</td>
      <td>Localidad de menos de 50 mil habitantes</td>
      <td>2</td>
      <td>15-001-11</td>
      <td>109313</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2</td>
      <td>5</td>
      <td>3</td>
      <td>47</td>
      <td>1286.0</td>
      <td>15001</td>
    </tr>
    <tr>
      <th>2</th>
      <td>150010000003</td>
      <td>15</td>
      <td>México</td>
      <td>001</td>
      <td>Acambay de Ruíz Castañeda</td>
      <td>0</td>
      <td>Localidad de menos de 50 mil habitantes</td>
      <td>2</td>
      <td>15-001-08</td>
      <td>109369</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1</td>
      <td>5</td>
      <td>1</td>
      <td>33</td>
      <td>4286.0</td>
      <td>15001</td>
    </tr>
    <tr>
      <th>3</th>
      <td>150010000004</td>
      <td>15</td>
      <td>México</td>
      <td>001</td>
      <td>Acambay de Ruíz Castañeda</td>
      <td>0</td>
      <td>Localidad de menos de 50 mil habitantes</td>
      <td>2</td>
      <td>15-001-03</td>
      <td>109375</td>
      <td>...</td>
      <td>6.0</td>
      <td>2.0</td>
      <td>4.0</td>
      <td>6.0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>39</td>
      <td>0.0</td>
      <td>15001</td>
    </tr>
    <tr>
      <th>4</th>
      <td>150010000005</td>
      <td>15</td>
      <td>México</td>
      <td>001</td>
      <td>Acambay de Ruíz Castañeda</td>
      <td>0</td>
      <td>Localidad de menos de 50 mil habitantes</td>
      <td>2</td>
      <td>15-001-04</td>
      <td>109402</td>
      <td>...</td>
      <td>5.0</td>
      <td>1.0</td>
      <td>3.0</td>
      <td>5.0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>39</td>
      <td>NaN</td>
      <td>15001</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 89 columns</p>
</div>




```python
persona_grand_mx= pd.concat([ pd.read_csv(df, encoding = 'latin_1', dtype={'ENTIDAD': str, 'MUN': str}) for df in  persona_zm_mexico])
persona_grand_mx['CODE_MUN'] = persona_grand_mx['ENT'] + persona_grand_mx['MUN'] 
persona_grand_mx.shape
persona_grand_mx.head()
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ID_VIV</th>
      <th>ID_PERSONA</th>
      <th>ENT</th>
      <th>NOM_ENT</th>
      <th>MUN</th>
      <th>NOM_MUN</th>
      <th>LOC50K</th>
      <th>NOM_LOC</th>
      <th>COBERTURA</th>
      <th>ESTRATO</th>
      <th>...</th>
      <th>HIJOS_FALLECIDOS</th>
      <th>HIJOS_SOBREVIV</th>
      <th>FECHA_NAC_M</th>
      <th>FECHA_NAC_A</th>
      <th>SOBREVIVENCIA</th>
      <th>EDAD_MORIR_D</th>
      <th>EDAD_MORIR_M</th>
      <th>EDAD_MORIR_A</th>
      <th>TAMLOC</th>
      <th>CODE_MUN</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>90020000001</td>
      <td>9002000000101</td>
      <td>09</td>
      <td>Distrito Federal</td>
      <td>002</td>
      <td>Azcapotzalco</td>
      <td>1</td>
      <td>Azcapotzalco</td>
      <td>2</td>
      <td>09-002-02</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5</td>
      <td>09002</td>
    </tr>
    <tr>
      <th>1</th>
      <td>90020000001</td>
      <td>9002000000102</td>
      <td>09</td>
      <td>Distrito Federal</td>
      <td>002</td>
      <td>Azcapotzalco</td>
      <td>1</td>
      <td>Azcapotzalco</td>
      <td>2</td>
      <td>09-002-02</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5</td>
      <td>09002</td>
    </tr>
    <tr>
      <th>2</th>
      <td>90020000002</td>
      <td>9002000000201</td>
      <td>09</td>
      <td>Distrito Federal</td>
      <td>002</td>
      <td>Azcapotzalco</td>
      <td>1</td>
      <td>Azcapotzalco</td>
      <td>2</td>
      <td>09-002-03</td>
      <td>...</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2.0</td>
      <td>1985.0</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5</td>
      <td>09002</td>
    </tr>
    <tr>
      <th>3</th>
      <td>90020000003</td>
      <td>9002000000306</td>
      <td>09</td>
      <td>Distrito Federal</td>
      <td>002</td>
      <td>Azcapotzalco</td>
      <td>1</td>
      <td>Azcapotzalco</td>
      <td>2</td>
      <td>09-002-02</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5</td>
      <td>09002</td>
    </tr>
    <tr>
      <th>4</th>
      <td>90020000003</td>
      <td>9002000000305</td>
      <td>09</td>
      <td>Distrito Federal</td>
      <td>002</td>
      <td>Azcapotzalco</td>
      <td>1</td>
      <td>Azcapotzalco</td>
      <td>2</td>
      <td>09-002-02</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5</td>
      <td>09002</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 87 columns</p>
</div>



## Filtrer les résultats


```python
vivienda_grand_mx['CODE_MUN']
```




    0         15001
    1         15001
    2         15001
    3         15001
    4         15001
    5         15001
    6         15001
    7         15001
    8         15001
    9         15001
    10        15001
    11        15001
    12        15001
    13        15001
    14        15001
    15        15001
    16        15001
    17        15001
    18        15001
    19        15001
    20        15001
    21        15001
    22        15001
    23        15001
    24        15001
    25        15001
    26        15001
    27        15001
    28        15001
    29        15001
              ...  
    214100    13084
    214101    13084
    214102    13084
    214103    13084
    214104    13084
    214105    13084
    214106    13084
    214107    13084
    214108    13084
    214109    13084
    214110    13084
    214111    13084
    214112    13084
    214113    13084
    214114    13084
    214115    13084
    214116    13084
    214117    13084
    214118    13084
    214119    13084
    214120    13084
    214121    13084
    214122    13084
    214123    13084
    214124    13084
    214125    13084
    214126    13084
    214127    13084
    214128    13084
    214129    13084
    Name: CODE_MUN, Length: 854816, dtype: object




```python
vivienda_zum_filtered = vivienda_grand_mx[vivienda_grand_mx['CODE_MUN'].isin(code_mun_zum)]
vivienda_zum_filtered.shape
```




    (442840, 89)




```python
vivienda_zum_filtered.head()
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ID_VIV</th>
      <th>ENT</th>
      <th>NOM_ENT</th>
      <th>MUN</th>
      <th>NOM_MUN</th>
      <th>LOC50K</th>
      <th>NOM_LOC</th>
      <th>COBERTURA</th>
      <th>ESTRATO</th>
      <th>UPM</th>
      <th>...</th>
      <th>ALIM_MEN3</th>
      <th>ING_ALIM_MEN1</th>
      <th>ING_ALIM_MEN2</th>
      <th>ING_ALIM_MEN3</th>
      <th>TAMLOC</th>
      <th>TIPOHOG</th>
      <th>JEFE_SEXO</th>
      <th>JEFE_EDAD</th>
      <th>INGTRHOG</th>
      <th>CODE_MUN</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3964</th>
      <td>150020000001</td>
      <td>15</td>
      <td>México</td>
      <td>002</td>
      <td>Acolman</td>
      <td>15</td>
      <td>Tepexpan</td>
      <td>2</td>
      <td>15-002-03</td>
      <td>109581</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5</td>
      <td>5</td>
      <td>3</td>
      <td>73</td>
      <td>NaN</td>
      <td>15002</td>
    </tr>
    <tr>
      <th>3965</th>
      <td>150020000002</td>
      <td>15</td>
      <td>México</td>
      <td>002</td>
      <td>Acolman</td>
      <td>15</td>
      <td>Tepexpan</td>
      <td>2</td>
      <td>15-002-03</td>
      <td>109570</td>
      <td>...</td>
      <td>6.0</td>
      <td>2.0</td>
      <td>4.0</td>
      <td>6.0</td>
      <td>5</td>
      <td>2</td>
      <td>3</td>
      <td>45</td>
      <td>13143.0</td>
      <td>15002</td>
    </tr>
    <tr>
      <th>3966</th>
      <td>150020000003</td>
      <td>15</td>
      <td>México</td>
      <td>002</td>
      <td>Acolman</td>
      <td>15</td>
      <td>Tepexpan</td>
      <td>2</td>
      <td>15-002-03</td>
      <td>109574</td>
      <td>...</td>
      <td>6.0</td>
      <td>2.0</td>
      <td>4.0</td>
      <td>6.0</td>
      <td>5</td>
      <td>1</td>
      <td>3</td>
      <td>37</td>
      <td>4286.0</td>
      <td>15002</td>
    </tr>
    <tr>
      <th>3967</th>
      <td>150020000004</td>
      <td>15</td>
      <td>México</td>
      <td>002</td>
      <td>Acolman</td>
      <td>0</td>
      <td>Localidad de menos de 50 mil habitantes</td>
      <td>2</td>
      <td>15-002-10</td>
      <td>109617</td>
      <td>...</td>
      <td>6.0</td>
      <td>2.0</td>
      <td>4.0</td>
      <td>6.0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>35</td>
      <td>18000.0</td>
      <td>15002</td>
    </tr>
    <tr>
      <th>3968</th>
      <td>150020000005</td>
      <td>15</td>
      <td>México</td>
      <td>002</td>
      <td>Acolman</td>
      <td>15</td>
      <td>Tepexpan</td>
      <td>2</td>
      <td>15-002-02</td>
      <td>109523</td>
      <td>...</td>
      <td>6.0</td>
      <td>2.0</td>
      <td>4.0</td>
      <td>6.0</td>
      <td>5</td>
      <td>1</td>
      <td>1</td>
      <td>42</td>
      <td>8000.0</td>
      <td>15002</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 89 columns</p>
</div>




```python
persona_zum_filtered = persona_grand_mx[persona_grand_mx['CODE_MUN'].isin(code_mun_zum)]
persona_zum_filtered.shape
```




    (1645437, 87)




```python
persona_zum_filtered.head()
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ID_VIV</th>
      <th>ID_PERSONA</th>
      <th>ENT</th>
      <th>NOM_ENT</th>
      <th>MUN</th>
      <th>NOM_MUN</th>
      <th>LOC50K</th>
      <th>NOM_LOC</th>
      <th>COBERTURA</th>
      <th>ESTRATO</th>
      <th>...</th>
      <th>HIJOS_FALLECIDOS</th>
      <th>HIJOS_SOBREVIV</th>
      <th>FECHA_NAC_M</th>
      <th>FECHA_NAC_A</th>
      <th>SOBREVIVENCIA</th>
      <th>EDAD_MORIR_D</th>
      <th>EDAD_MORIR_M</th>
      <th>EDAD_MORIR_A</th>
      <th>TAMLOC</th>
      <th>CODE_MUN</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>90020000001</td>
      <td>9002000000101</td>
      <td>09</td>
      <td>Distrito Federal</td>
      <td>002</td>
      <td>Azcapotzalco</td>
      <td>1</td>
      <td>Azcapotzalco</td>
      <td>2</td>
      <td>09-002-02</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5</td>
      <td>09002</td>
    </tr>
    <tr>
      <th>1</th>
      <td>90020000001</td>
      <td>9002000000102</td>
      <td>09</td>
      <td>Distrito Federal</td>
      <td>002</td>
      <td>Azcapotzalco</td>
      <td>1</td>
      <td>Azcapotzalco</td>
      <td>2</td>
      <td>09-002-02</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5</td>
      <td>09002</td>
    </tr>
    <tr>
      <th>2</th>
      <td>90020000002</td>
      <td>9002000000201</td>
      <td>09</td>
      <td>Distrito Federal</td>
      <td>002</td>
      <td>Azcapotzalco</td>
      <td>1</td>
      <td>Azcapotzalco</td>
      <td>2</td>
      <td>09-002-03</td>
      <td>...</td>
      <td>0.0</td>
      <td>5.0</td>
      <td>2.0</td>
      <td>1985.0</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5</td>
      <td>09002</td>
    </tr>
    <tr>
      <th>3</th>
      <td>90020000003</td>
      <td>9002000000306</td>
      <td>09</td>
      <td>Distrito Federal</td>
      <td>002</td>
      <td>Azcapotzalco</td>
      <td>1</td>
      <td>Azcapotzalco</td>
      <td>2</td>
      <td>09-002-02</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5</td>
      <td>09002</td>
    </tr>
    <tr>
      <th>4</th>
      <td>90020000003</td>
      <td>9002000000305</td>
      <td>09</td>
      <td>Distrito Federal</td>
      <td>002</td>
      <td>Azcapotzalco</td>
      <td>1</td>
      <td>Azcapotzalco</td>
      <td>2</td>
      <td>09-002-02</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>5</td>
      <td>09002</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 87 columns</p>
</div>




```python
differences = pd.DataFrame({'Non filtré':[persona_grand_mx.shape[0], vivienda_grand_mx.shape[0]],
                            'Filtré':[persona_zum_filtered.shape[0], vivienda_zum_filtered.shape[0]],
                           'Différence':[persona_grand_mx.shape[0] - persona_zum_filtered.shape[0],
                                         vivienda_grand_mx.shape[0] - vivienda_zum_filtered.shape[0]] 
                           }, {'persona', 'vivienda'})
differences
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Différence</th>
      <th>Filtré</th>
      <th>Non filtré</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>persona</th>
      <td>1645080</td>
      <td>1645437</td>
      <td>3290517</td>
    </tr>
    <tr>
      <th>vivienda</th>
      <td>411976</td>
      <td>442840</td>
      <td>854816</td>
    </tr>
  </tbody>
</table>
</div>



### fusion des bases VIVIENDA et PERSONA


```python
result = pd.merge(persona_zum_filtered, vivienda_zum_filtered, on='ID_VIV')
result.shape
```




    (1645437, 175)



## enregistrement dans un csv


```python
result.to_csv('persona_vivienda_zm_mexico.csv', sep=',', encoding='utf-8')
```

## Compression du fichier


```bash
%%bash 
7za -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on a ./persona_vivienda_zm_mexico.7z ./persona_vivienda_zm_mexico_2010.csv
```

    
    7-Zip (A) [64] 9.20  Copyright (c) 1999-2010 Igor Pavlov  2010-11-18
    p7zip Version 9.20 (locale=fr_FR.UTF-8,Utf16=on,HugeFiles=on,4 CPUs)
    Scanning
    
    Creating archive ./persona_vivienda_zm_mexico.7z
    
    Compressing  persona_vivienda_zm_mexico_2010.csv
    
    Everything is Ok

