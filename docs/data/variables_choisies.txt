variable,explication
ID_PERSONA,identifiant de la personne
ID_VIV,identifiant de l'habitation (base PERSONA)
ENT_x,code Entidad Federativa
NOM_ENT_x, nom Entidad Federativa
MUN_x, code Municipio
NOM_MUN_x, Nom Municipio 
LOC50K_x, code localidad
NOM_LOC_x, code localidad
FACTOR_x,facteur de pondération de la persona
FACTOR_y,facteur de pondération de la vivienda
CLAVIVP,type logement
PISOS,type de sols
TOTCUART,nombre de pièces
CUADORM,nombre de chambres
COMBUSTIBLE,type de chauffage
ELECTRICIDAD,Electricité
AGUA_ENTUBADA,Arrivée d'eau
ABA_AGUA_ENTU,approvisionnement en eau
ABA_AGUA_NO_ENTU,approvisionnement en eau
AIRE_ACON,air conditionné
SERSAN,toilettes
USOEXC,toilettes partagés
DRENAJE,evacuation des eaux usées
DESTINO_BASURA,traitement des ordures ménagères
REFRIGERADOR,réfrigérateur
LAVADORA,lavabo
HORNO,four
AUTOPROP,voiture
RADIO,récepteur radio
TELEVISOR,télévision
TELEVISOR_PP,télévision à écran plat
COMPUTADORA,ordinateur
TELEFONO,ligne de téléphone terrestre
CELULAR, téléphone mobile
INTERNET,accès internet
TENENCIA,propiétaire du logement
NUMPERS,nombre de résidents du logement
TIPOHOG,type de foyer
PERTE_INDIGENA,se considère comme indigène
HLENGUA,parle un dialecte indigène
HESPANOL,parle espagnol
NIVACAD,niveau de scolarité
ALFABET,alphabétisation
CONACT,statut d'activité
