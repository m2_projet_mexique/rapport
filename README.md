This is a study who aims to collect and process big databases. The subject is Mexico urban data, and use data from the UNEGI.

## Contribute

To contribute to this project, please take a look at the [Contributing Guidelines](CONTRIBUTING.md) first. Please note that this project is released with a [Contributor Code of Conduct](CONDUCT.md). By participating in this project you agree to abide by its terms.

## Based on liftr
liftr  <a href="https://liftr.me"><img src="https://i.imgur.com/3SCYZu0.png" align="right" alt="logo" height="180" width="180" /></a>
liftr is free and open source software, licensed under GPL-3.

<img src="https://i.imgur.com/AKveypK.png" width="100%" alt="Containerize R Markdown Documents with liftr">
